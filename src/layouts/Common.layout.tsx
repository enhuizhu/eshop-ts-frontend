import React from 'react';
import { layout } from '../jss/layout';
import { base } from '../jss/base';
import { connect } from 'react-redux';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { UtilService } from '../services/UtilService';
import { Icon } from '@material-ui/core';
import { Header } from '../components/header/Header';
import { Footer } from '../components/footer/Footer';

const useStyles = makeStyles((theme: Theme) => createStyles({
  ...layout(theme),
  ...base,
}));

const Common: React.FC = (props: any) => {
  const classes = useStyles();
  const { shopData } = props;
  const logo = UtilService.getValueBaseProperty(shopData, ['logo']);

  return (<div className={classes.layoutContainer}>
    <div className={classes.header}>
      <Header>
        <div className='img-wrapper'>
          {
            logo ?
              (<React.Fragment>
                <img src={`${process.env.REACT_APP_API_PATH}uploads/${logo}`}></img><br/>
              </React.Fragment>): ''
          }
        </div>
        <h3>{UtilService.getValueBaseProperty(shopData, ['shopName'])}</h3>
        <h4><Icon>phone</Icon>0161 205 0688</h4>
        <h4><Icon>room</Icon>98 Church Ln, Manchester M9 4NH</h4>
      </Header>
    </div>
    <div className={classes.mainContent}>
      {props.children}
    </div>
    <div className={classes.footer}>
      <Footer></Footer>
    </div>
  </div>)
};

function mapStateToProps(state: any) {
  return {
    shopData: state.shopData,
  }
}

const CommonLayoutContainer = connect(mapStateToProps)(Common);

export default CommonLayoutContainer;
