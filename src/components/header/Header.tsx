import React from 'react';
import { createStyles, Theme, makeStyles, withStyles } from '@material-ui/core/styles';
import { Modal, Paper, Icon, Menu, MenuItem } from '@material-ui/core';
import { Login } from '../login/Login';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { removeUserInfo } from '../../actions/userinfo.action';
import { withRouter } from 'react-router';

const ws = withStyles((theme: Theme) => ({
  navigation: {
    color: 'white',
    zIndex: 10,
    right: 10,
    top: -12,
    position: 'absolute' as 'absolute',
    listStyle: 'none',
    '& li': {
      float: 'left' as 'left',
      marginRight: 17,
      fontSize: 14,
      cursor: 'pointer'
    }
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    width: '90%',
    maxWidth: 500,
    padding: 10,
  }
}));

export class HeaderOriginal extends React.Component<any, any> {
  private navs = [
    {
      label: 'Home',
      onClick: () => {
        this.props.history.push('/');
      }
    },
    {
      label: 'Register',
      onClick: () => {
        this.props.history.push('/register');
      }
    },
    {
      label: 'Login',
      onClick: () => {
        this.setState({
          loginModalOpen: true,
        });
      }
    }
  ];

  public state = {
    loginModalOpen: false,
    anchorEl: null,
  }
  
  constructor(props: any) {
    super(props)
  }

  handelLoginModalOpen() {
    this.setState({
      loginModalOpen: false,
    })
  }

  openLoginModal() {
    this.setState({
      loginModalOpen: true,
    })
  }

  handleClick = (event: React.MouseEvent<HTMLLabelElement>) => {
    this.setState({
      anchorEl: event.currentTarget
    });
  };

  handleClose = () => {
    this.setState({
      anchorEl: null,
    });
  }

  handleLogout = () => {
    setTimeout(() => {
      this.props.removeUserInfo();
      this.handleClose();
    }, 500);
  }

  getNav = (userInfo: any, classes: any) => {
    if (isEmpty(userInfo)) {
      return <ul className={classes.navigation}>
        {this.navs.map((nav, index) => {
          return (
            <li key={index} onClick={nav.onClick.bind(this)}>
              {nav.label}
            </li>
          );
        })}
      </ul>;
    } else {
      return <React.Fragment>
        <ul className={classes.navigation}>
          {
            this.navs.filter(nav => {
              return (nav.label !== 'Register' 
                && nav.label !== 'Login');
            }).map((nav, index) => {
              return (
                <li key={index} onClick={nav.onClick.bind(this)} style={{paddingTop: 5}}>
                  {nav.label}
                </li>
              );
            })
          }
          <li>
            <label
              aria-controls="userMenu"
              aria-haspopup="true" 
              onClick={this.handleClick} 
              style={{cursor: 'pointer'}}>
              <Icon>person</Icon>
              {this.props.userInfo.username}
            </label>
            <Menu
              id="userMenu"
              anchorEl={this.state.anchorEl}
              keepMounted
              open={Boolean(this.state.anchorEl)}
              onClose={this.handleClose}
            >
              <MenuItem onClick={this.handleClose}>Profile</MenuItem>
              <MenuItem onClick={this.handleClose}>My account</MenuItem>
              <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
            </Menu>
          </li>
        </ul>
      </React.Fragment>;
    }
  }

  render() {
    const { classes, userInfo } = this.props;

    return <React.Fragment>
      { this.getNav(userInfo, classes) }
      <div className='overlay'></div>
      <div className='info'>
        {this.props.children}
      </div>
      <Modal
        aria-labelledby="login-modal-title"
        aria-describedby="login-modal-description"
        open={this.state.loginModalOpen && isEmpty(userInfo)}
        onClose={this.handelLoginModalOpen.bind(this)}
        className={classes.modal}
      >
        <Paper className={classes.paper}>
          <Login onLogin={() => {
            this.setState({loginModalOpen: false});
          }}></Login>
        </Paper>
      </Modal>
    </React.Fragment>;
  }
};

const mapStateToProps = (state: any) => {
  return {
    userInfo: state.userInfo
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    removeUserInfo: () => {
      dispatch(removeUserInfo());
    }
  }
}

export const Header = ws(
  connect(mapStateToProps, mapDispatchToProps)(
    withRouter(HeaderOriginal)
  )
);
