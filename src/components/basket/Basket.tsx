import React from 'react';
import Card from '@material-ui/core/Card';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { Icon, Divider, Radio, List, ListItem, ListItemText, ListItemAvatar } from '@material-ui/core';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import { WaysOfGettingFood } from '../../constants/waysOfGettingFood.constant';
import { removeFromBasket, changeWayOfGettingFood } from '../../actions/basket.action';
import { connect } from 'react-redux';
import { UtilService } from '../../services/UtilService';
import { base } from '../../jss/base';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    ...base,
    header: {
      background: theme.palette.grey[50],
      padding: 10,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      '& .title': {
        fontWeight: 'bold' as 'bold',
      }
    },
   
    priceList: {
      '& .MuiTypography-body1': {
        fontSize: 13,
      }
    },
  })
);

export const Basket = function(props: any) {
  let total = 0;
  const deliverFee = 1;
  
  const classes = useStyles();
  const { items, wayOfGettingFood, buttonText, buttonClick, className } = props;

  const handleChange = (e: any) => {
    props.changeWayOfGettingFood(e.target.value);
  }

  return (
    <Card variant="outlined" className={className}>
      <div className={classes.header}>
        <span className="title">Your Order</span>
        <Icon>shopping_cart_outlined</Icon>
      </div>
      <div className={classes.paddingTopBottom10}>
        <List component="nav" aria-label="main mailbox folders">
          {
            items && items.map((item: any, index: number) => {
              const price = item.price * item.quantity;
              total += price;
              
              return <ListItem button key={index} style={{position: 'relative'}}>
                <ListItemAvatar 
                  style={{width: 28, minWidth: 'auto'}}
                >
                  <Icon 
                    color='secondary'
                    onClick={() => {
                      props.removeFromBasket(item);
                    }}
                  >highlight_off</Icon>
                </ListItemAvatar>
                <ListItemText primary={item.name} />
                <ListItemText 
                  primary={UtilService.formatAsSterling(price)}
                  className={classes.absRight}
                >
                </ListItemText>
              </ListItem>
            })
          }
        </List>
      </div>
      <Divider></Divider>
      <FormControl 
        component="fieldset" 
      >
        <RadioGroup
          className={classes.paddingLeft10}
          aria-label="position" 
          name="position" 
          value={wayOfGettingFood} 
          onChange={handleChange} row
        >
          <FormControlLabel
            value={WaysOfGettingFood.DELIVERY}
            control={<Radio color="default" size="small"/>}
            label={WaysOfGettingFood.DELIVERY}
            labelPlacement="end"
          />
          <FormControlLabel
            value={WaysOfGettingFood.TAKE_AWAY}
            control={<Radio color="default" size="small"/>}
            label={WaysOfGettingFood.TAKE_AWAY}
            labelPlacement="end"
          />
        </RadioGroup>
      </FormControl>
      <Divider></Divider>
      <div className={classes.paddingTopBottom10}>
        <List>
          <ListItem className={classes.priceList}>
            <ListItemText primary='Subtotal'></ListItemText>
            <ListItemText primary={UtilService.formatAsSterling(total)} className={classes.absRight}></ListItemText>
          </ListItem>
          <ListItem className={classes.priceList}>
            <ListItemText primary='Delivery fee'></ListItemText>
            <ListItemText primary={UtilService.formatAsSterling(deliverFee)} className={classes.absRight}></ListItemText>
          </ListItem>
          <ListItem className={classes.priceList}>
            <ListItemText primary='Total'></ListItemText>
            <ListItemText primary={UtilService.formatAsSterling(total + deliverFee)} className={classes.absRight}></ListItemText>
          </ListItem>
        </List>
      </div>
      <Divider></Divider>
      <div 
        className={`${classes.paddingTopBottom10} ${classes.centerAlignment}`}
      >
        <Button size='large' variant="contained" color='secondary' onClick={buttonClick}>
          { buttonText }
        </Button> 
      </div>
    </Card>
  );
}

function mapStateToProps(state: any) {
  const items = state.basket.get('items');
  const wayOfGettingFood = state.basket.get('waysOfGettingFood');

  return {
    items,
    wayOfGettingFood,
  }
}

function mapDispatchToProps(dispatch: any) {
  return {
    removeFromBasket: (p: any) => {
      dispatch(removeFromBasket(p));
    },
    changeWayOfGettingFood: (wayOfGettingFood: string) => {
      dispatch(changeWayOfGettingFood(wayOfGettingFood));
    }
  }
}

export const BasketContainer = connect(mapStateToProps, mapDispatchToProps)(Basket);

