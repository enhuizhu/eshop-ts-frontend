import React from 'react';
import Card from '@material-ui/core/Card';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from '@material-ui/core/Icon';
import Divider from '@material-ui/core/Divider';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { UtilService } from '../../services/UtilService';

export const CategoryList = (props: any) => {
  const { categories, className } = props;

  return <Card variant="outlined" className={className}>
    <List 
      component="nav" 
      aria-label="main mailbox folders"
    >
      { 
        categories &&
        categories.map((d: any, index: number) => {
          return (
            <React.Fragment key={index}>
              {
                index === 0 ? '' : <Divider></Divider>
              }
              <ListItem button>
                <ListItemText 
                  primary={UtilService.upFirst(d.name)}
                />
                <ListItemIcon>
                  <Icon style={{fontSize: '10px'}}>arrow_forward_ios_sharp</Icon>
                </ListItemIcon>
              </ListItem>
            </React.Fragment>
          );
        })
      }
    </List>
  </Card>;
}