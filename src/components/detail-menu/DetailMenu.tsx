import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { UtilService } from '../../services/UtilService';
import IconButton from '@material-ui/core/IconButton';
import { ListItemSecondaryAction, Icon } from '@material-ui/core';
import { addToBasket, removeFromBasket } from '../../actions/basket.action';
import { connect } from 'react-redux';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: 'inline',
    },
    headerItem: {
      width: "45%",
    },
    description: {
      width: "45%"
    },
    listItem: {
      background: theme.palette.grey[50],
    }
  }),
);

export const DetailMenu = function(props: any) {
  const classes = useStyles();
  const { category } = props;

  return (
    <div>
      <h3>{ UtilService.upFirst(category.name) }</h3>
      <List className={classes.root}>
        <ListItem>
          <ListItemAvatar>
            <span></span>
          </ListItemAvatar>
          <ListItemText primary={'Item'} className={classes.headerItem}></ListItemText>
          <ListItemText primary={'Price'}></ListItemText>
          <ListItemSecondaryAction>
            Order
          </ListItemSecondaryAction>
        </ListItem>
        <Divider component="li" />
        {
          category.products.map((p: any, index: number) => {
            return (
              <React.Fragment key={index}>
                <ListItem alignItems="flex-start" className={index % 2 === 0 ? classes.listItem : ''}>
                  <ListItemAvatar>
                    <img style={{width: '50px'}} alt={p.name} src={`${process.env.REACT_APP_API_PATH}uploads/${UtilService.getValueBaseProperty(p, ['pics'])}`} />
                  </ListItemAvatar>
                  <ListItemText
                    className={classes.description}
                    primary={p.name}
                    secondary={
                      <React.Fragment>
                        <Typography
                          component="span"
                          variant="body2"
                          className={classes.inline}
                          color="textPrimary"
                        >
                        </Typography>
                        {p.description}
                      </React.Fragment>
                    }
                  />
                  <ListItemText
                    primary={UtilService.formatAsSterling(p.price)}
                    style={{paddingTop: 12}}
                  >
                </ListItemText>
                <ListItemSecondaryAction>
                  <IconButton edge="end" aria-label="add" onClick={() => {
                    props.addToBasket(p);
                  }}>
                    <Icon color="secondary">add_circle</Icon>    
                  </IconButton>
                </ListItemSecondaryAction>
                </ListItem>
                <Divider component="li" />
              </React.Fragment>
            );
          })
        }
      </List>
    </div>
  );
}

function mapDispatchToProps(dispatch: any) {
  return {
    addToBasket: (p: any) => {
      dispatch(addToBasket(p));
    }
  }
}

DetailMenu.propTypes = {
  category: PropTypes.object
};

DetailMenu.defaultProps = {
  category: {}
};

export const DetailMenuContainer = connect(null, mapDispatchToProps)(DetailMenu);
