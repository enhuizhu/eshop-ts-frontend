import React from 'react';
import { Theme, makeStyles, withStyles } from '@material-ui/core/styles';
import { TextField, Button } from '@material-ui/core';
import { base } from '../../jss/base';
import { ApiService } from '../../services/ApiService';
import store from '../../store/store';
import { showLoader, hideLoader } from '../../actions/loader.actions';
import NotificationService from '../../services/NotificationService';
import { ERROR, SUCCESS, NOTIFICATION } from '../../constants/notification.constant';
import { receiveUserinfo } from '../../actions/userinfo.action';
import { Link } from 'react-router-dom';

const ws = withStyles((theme: Theme) => 
  ({
    ...base,
  })
);

class LoginOriginal extends React.Component<any, any> {
  public state = {
    username: null,
    usernameError: false,
    password: null,
    passwordError: false,
  };

  constructor(props: any) {
    super(props);
  }

  handelSubmit = (e: any) => {
    e.preventDefault();

    let hasError = false;
    
    if (!this.state.username) {
      this.setState({usernameError: true});
      hasError = true;
    }

    if (!this.state.password) {
      this.setState({passwordError: true});
      hasError = true;
    }

    if (hasError) {
      return ;
    }

    store.dispatch(showLoader());
    // everything is good, need to call the login API
    ApiService.login({username: this.state.username, password: this.state.password})
      .then(res => {
        console.log('res', res);
        if (!res.success) {
          NotificationService.pub(NOTIFICATION, {
            msgType: ERROR,
            msg: res.message,
          });
        } else {
          // need to save the userInfo in store and session storage
          store.dispatch(receiveUserinfo(res));
          this.props.onLogin();
        }
      }).catch(e => {
        console.log('error', e);
        NotificationService.pub(NOTIFICATION, {
          msgType: ERROR,
          msg: 'error on login'
        });
      }).finally(() => {
        store.dispatch(hideLoader());
      })  
  } 

  render() {
    const { classes } = this.props; 
  
    return (
      <React.Fragment>
        <form onSubmit={this.handelSubmit}>
          <TextField 
            label="Username or Email"
            placeholder="Username or Email"
            error={this.state.usernameError}
            defaultValue={this.state.username}
            onFocus={() => {
              this.setState({
                usernameError: false,
              });
            }}
            onChange={(e) => {
              this.setState({
                username: e.target.value
              });
            }}
            fullWidth 
            className={classes.marginBottom10}
          />
          <TextField 
            label="Password"
            placeholder="Password"
            type="password"
            error={this.state.passwordError}
            defaultValue={this.state.password}
            onFocus={() => {
              this.setState({
                passwordError: false,
              });
            }}
            onChange={(e) => {
              this.setState({
                password: e.target.value
              });
            }}
            fullWidth
            className={classes.marginBottom10} 
          />
          <Button 
            variant="contained" 
            color="primary"
            className={classes.marginBottom10}
            style={{width: '100%'}}
            type="submit"
            onClick={this.handelSubmit}
            disableElevation>
            Sign In
          </Button>
          <div className={classes.marginBottom10}>
            <span className={classes.pullLeft}>
              <Link to='/forget-password'>Forget Password?</Link>
            </span>
            <span className={classes.pullRight}>
              Don't have an account? 
              <Link to='/register'>Sign Up</Link>
            </span>
          </div>
        </form>
      </React.Fragment>
    );
  }
}

export const Login = ws(LoginOriginal);

