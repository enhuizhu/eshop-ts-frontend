import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { layout } from '../../jss/layout';
import { connect } from 'react-redux';
import { UtilService } from '../../services/UtilService';

const useStyles = makeStyles((theme: Theme) => createStyles({
  ...layout(theme),
  listTitle: {
    width: 78
  }
}));

export const OpenHours = (props: any) => {
  const { openHours } = props;
  const classes = useStyles();

  console.log('openhours', openHours);
  let openTimes = UtilService.getValueBaseProperty(openHours, ['openTime']) || {};

  if (openTimes && typeof openTimes === 'string') {
    openTimes = JSON.parse(openTimes);
  }

  console.log('open times', openTimes);

  return (<Card variant='outlined' className='list'>
    <div className={classes.menuTitle}>
      Business Hours
    </div>
    <List>
      {
        Object.keys(openTimes).map(k => {
          return (
            <ListItem key={k}>
              <ListItemText primary={k} className={classes.listTitle}></ListItemText>
              <ListItemText>{openTimes[k].close ? 'off' : `${openTimes[k].start} - ${openTimes[k].end}`}</ListItemText>
            </ListItem>
          );
        })
      }
    </List>
  </Card>);
}

function mapStateToProps(state: any) {
  return {
    openHours: state.businessHours
  }
}

export default connect(mapStateToProps)(OpenHours);

