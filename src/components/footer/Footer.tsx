import React from 'react';
import { Link } from '@material-ui/core';

export const Footer = function() {
  return (<React.Fragment>
      Copyright © 2019 <Link href='http://www.olmarket.co.uk'>Online Marketing Solution</Link>
  </React.Fragment>);    
}