import store from './store';
import { RECEIVE_SHOP_DATA } from '../actions/shopData.action'
import { WaysOfGettingFood } from '../constants/waysOfGettingFood.constant';
import { 
  addToBasket, 
  removeFromBasket,
  changeWayOfGettingFood 
} from '../actions/basket.action';
import {
  receiveUserinfo
} from '../actions/userinfo.action';
import { RECEIVE_BUSINESS_HOURS_DATA } from '../actions/businessHours.action';

describe('test store situation', () => {
  it('test initial state of store', () => {
    store.dispatch({type: RECEIVE_SHOP_DATA, payload: {logo: 'a.jpg', name: 'wok express'}});
    expect(store.getState().shopData).toEqual({ logo: 'a.jpg', name: 'wok express' });
  });

  it('test items in basket', () => {
    let state = store.getState();
    expect(state.basket.get('items')).toEqual([]);
    
    const p = {id: 1};

    store.dispatch(addToBasket(p));
    state = store.getState();
    expect(state.basket.get('items')).toEqual([{id: 1, quantity: 1}]);

    store.dispatch(addToBasket(p));
    state = store.getState();
    expect(state.basket.get('items')).toEqual([{id: 1, quantity: 2}]);

    store.dispatch(removeFromBasket(p));
    state = store.getState();
    expect(state.basket.get('items')).toEqual([]);
  });

  it('test ways of getting food in basket', () => {
    let state = store.getState();
    expect(state.basket.get('waysOfGettingFood')).toEqual(WaysOfGettingFood.DELIVERY);
    store.dispatch(changeWayOfGettingFood(WaysOfGettingFood.TAKE_AWAY));
    state = store.getState();
    expect(state.basket.get('waysOfGettingFood')).toEqual(WaysOfGettingFood.TAKE_AWAY);
  });

  it('test userInfo reducer', () => {
    let state = store.getState();
    expect(state.userInfo).toEqual({});

    store.dispatch(receiveUserinfo({token: 'abc', username: 'enhuizhu'}));
    state = store.getState();
    expect(state.userInfo).toHaveProperty('token');
    expect(state.userInfo).toHaveProperty('username');
  });

  it('test business hours reducer', () => {
    let state = store.getState();
    expect(state.businessHours).toEqual({});

    store.dispatch({type: RECEIVE_BUSINESS_HOURS_DATA, payload: {id: 1}});
    state = store.getState();
    expect(state.businessHours).toEqual({id: 1});
  })
});