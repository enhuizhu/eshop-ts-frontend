import { UtilService } from './UtilService';

describe('UtilService', () => {
  it('upFirst', () => {
    expect(UtilService.upFirst('hello')).toBe('Hello');
  });

  it('getValueBaseProperty', () => {
    let obj: any = {a: 1};
    expect(UtilService.getValueBaseProperty(obj, ['a'])).toBe(1);
    obj = {a: {b: 2}};
    expect(UtilService.getValueBaseProperty(obj, ['a', 'b'])).toBe(2);
    obj = {a: {b: {c: 3}}};
    expect(UtilService.getValueBaseProperty(obj, ['a', 'b', 'c'])).toBe(3);
  });
});