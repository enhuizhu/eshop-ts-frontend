import { number } from "prop-types";

export class UtilService {
  static upFirst(input: String): String {
    return input.substr(0, 1).toUpperCase() + input.substr(1);
  }

  static formatAsSterling(price: number): String {
    return '£' + price.toFixed(2);
  }

  static getValueBaseProperty(obj: any, properties: any[]) {
    let index = 0;
    let p = properties[0];
    let o = obj[p];
    
    while(o && p && index < properties.length - 1) {
      p = properties[++index];
      o = o[p];
    }

    return o;
  }
}

