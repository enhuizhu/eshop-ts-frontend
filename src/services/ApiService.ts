import { HttpService } from './HttpService';
import store from '../store/store';

export class ApiService {
  static getShopData() {
    return HttpService.get(this.getPath('products-with-token'));
  }

  static login(userInfo: any) {
    return HttpService.post(this.getPath('customer/login'), userInfo);
  }

  static register(userInfo: any) {
    return HttpService.post(this.getPath('customer/register'), userInfo);
  }

  static placeOrder(orderInfo: any) {
    const userInfo = store.getState().userInfo;

    if (!userInfo || !userInfo.token) {
      throw new Error('user token is empty, please login first');
    }

    return HttpService.post(this.getPath(`/customer/placeOrder/?token=${userInfo.token}`), orderInfo);
  }

  static getBusinessHours() {
    return HttpService.get(this.getPath('business/open-hours'));
  }
  
  static getPath(path: string): string {
    return `${process.env.REACT_APP_API_PATH}${path}`;
  }
}
