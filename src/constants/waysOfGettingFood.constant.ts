export enum WaysOfGettingFood {
  DELIVERY = 'Delivery',
  TAKE_AWAY = 'Take Away'
};
