import React, { Component } from 'react';
import './App.scss';
import Home from './pages/home/Home';
import store from './store/store';
import { Provider } from 'react-redux';
import { getShopData } from './actions/shopData.action';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Order } from './pages/order/Order';
import { withRouter } from 'react-router';
import Notification from './components/notification/Notification';
import { LoaderContainer } from './components/loader/Loader';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { Register } from './pages/register/Register';
import { ForgetPassword } from './pages/forget-password/ForgetPassword';

const theme = createMuiTheme({
  overrides: {
    MuiListItemText: {
      root: {
        fontSize: 13,
      }
    },
    MuiTypography: {
      root: {
        fontSize: 13,
      },
      body1: {
        fontSize: 13,
      }
    }

  }
});

class App extends Component {
  componentDidMount() {
    store.dispatch(getShopData());
  }
  
  render() {
    return (
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <Router>
            <Route path='/' component={Home} exact></Route>
            <Route path='/order' component={withRouter(Order)}></Route>
            <Route path='/register' component={withRouter(Register)}></Route>
            <Route path='/forget-password' component={withRouter(ForgetPassword)}></Route>
          </Router>

          <Notification></Notification>
          <LoaderContainer></LoaderContainer>
        </Provider>
      </ThemeProvider>
    );
  }
}

export default App;
