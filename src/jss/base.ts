export const base = {
  paddingLeft10: {
    paddingLeft: 10,
  },
  paddingTopBottom10: {
    padding: '10px 0',
  },
  padding10: {
    padding: 10,
  },
  marginBottom10: {
    marginBottom: 20,
  },
  marginTop10: {
    marginTop: 10,
  },
  absRight: {
    position: 'absolute' as 'absolute',
    right: 10,
    top: 9,
  },
  centerAlignment: {
    textAlign: 'center' as 'center',
  },
  pullLeft: {
    float: 'left' as 'left',
  },
  pullRight: {
    float: 'right' as 'right',
  },
  clearFix: {
    clear: 'both' as 'both',
  }
}