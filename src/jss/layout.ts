import { Theme } from '@material-ui/core/styles';
import { NONAME } from 'dns';

export const layout = (theme: Theme) => ({
  'header': {
    'grid-area': 'header',
    'height': '300px',
    'background-image': 'url(header-bg.jpg)',
    'position': 'relative' as 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    '& .material-icons': {
      fontSize: '18px',
      position: 'relative' as 'relative',
      top: '3px',
      marginRight: '3px',
    },
    '& .overlay': {
      width: '100%',
      height: '100%',
      position: 'absolute' as 'absolute',
      top: 0,
      left: 0,
      'background-color': 'rgb(0, 0, 0, 0.7)'
    },
    '& .info': {
      position: 'relative' as 'relative',
      textAlign: 'center' as 'center',
      color: 'white',
      '& .img-wrapper': {
        background: 'white',
        padding: '10px',
        width: '117px',
        margin: 'auto',
      },
      
      '& img': {
        width: '100px'
      }
    }
  }, 
  'leftCategories': {
    'grid-area': 'leftCategories',
    paddingLeft: 10,
    [theme.breakpoints.down('sm')]: {
      padding: '10px 15px',
    }
  },
  mainContentItem: {
    paddingTop: 10,
  },
  'menu': {
    'grid-area': 'menu',
    padding: '10px 15px 0 15px',
  },
  breadcrumbs: {
    'grid-area': 'breadcrumbs',
    background: theme.palette.background.paper,
    padding: 10,
  },
  menuTitle: {
    background: theme.palette.primary.light,
    padding: 10,
    fontWeight: 'bold' as 'bold',
    fontSize: 18,
    color: 'white'
  },
  'basket': {
    'grid-area': 'basket',
    paddingRight: 10,
    [theme.breakpoints.down('sm')]: {
      '&': {
        padding:'20px 16px',
      }
    }
  },
  'footer': {
    'grid-area': 'footer',
    textAlign: "center" as "center",
    padding: '20px 0'
  },
  'appContainer' : {
    display: 'grid',
    'grid-template-areas': `
      'header header header header'
      'breadcrumbs breadcrumbs breadcrumbs breadcrumbs'
      'leftCategories menu menu basket'
      'footer footer footer footer'
    `,
    [theme.breakpoints.down('sm')]: {
      'grid-template-areas': `
        'header' 
        'breadcrumbs' 
        'leftCategories' 
        'menu' 
        'basket' 
        'footer'
      `,
      gridTemplateColumns: '100%',
    },
    gridTemplateColumns: '25% 25% 25% 25%',
  },
  mainContent: {
    'grid-area': 'mainContent',
  },
  layoutContainer: {
    display: 'grid',
    'grid-template-areas': `
      'header'
      'mainContent'
      'footer'
    `,
  },
});
