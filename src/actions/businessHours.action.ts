import { ApiService } from '../services/ApiService';

export const RECEIVE_BUSINESS_HOURS_DATA = 'RECEIVE_BUSINESS_HOURS_DATA';

export const getBusinessHours: any  = () => {
  return (dispatch: any) => {
    ApiService.getBusinessHours().then(response => {
      console.log('business hours response', response);
      dispatch({
        type: RECEIVE_BUSINESS_HOURS_DATA,
        payload: response,
      })
    }).catch(console.error)
  }
}
