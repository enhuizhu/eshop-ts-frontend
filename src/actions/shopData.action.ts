import { ApiService } from '../services/ApiService';
import { showLoader, hideLoader } from './loader.actions';

export const RECEIVE_SHOP_DATA = 'RECEIVE_SHOP_DATA';

export const getShopData: any = () => {
  return (dispatch: any) => {
    dispatch(showLoader());

    ApiService.getShopData().then(response => {
      console.log('shop reducer response', response);
      dispatch({
        type: RECEIVE_SHOP_DATA,
        payload: response
      });
    })
    .catch(console.error)
    .finally(() => {
      dispatch(hideLoader());
    });
  };
};
