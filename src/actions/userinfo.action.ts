export const RECEIVE_USERINFO = 'RECEIVE_USERINFO';
export const REMOVE_USERINFO = 'REMOVE_USERINFO';

export const receiveUserinfo = (payload: any) => ({
  type: RECEIVE_USERINFO,
  payload  
});

export const removeUserInfo = () => ({
  type: REMOVE_USERINFO
});
