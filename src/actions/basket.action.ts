export const ADD_TO_BASKET = 'ADD_TO_BASKET';
export const REMOVE_FROM_BASKET = 'REMOVE_FROM_BASKET';
export const CHANGE_WAY_OF_GETTING_FOOD = 'CHANGE_WAY_OF_GETTING_FOOD';

export const addToBasket = (payload: any) => ({
  type: ADD_TO_BASKET,
  payload,
});

export const removeFromBasket = (payload: any) => ({
  type: REMOVE_FROM_BASKET,
  payload,
});

export const changeWayOfGettingFood = (payload: string) => ({
  type: CHANGE_WAY_OF_GETTING_FOOD,
  payload,
});
