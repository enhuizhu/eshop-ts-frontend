import { ADD_TO_BASKET , REMOVE_FROM_BASKET, CHANGE_WAY_OF_GETTING_FOOD } from '../actions/basket.action';
import { Map } from 'immutable';
import { WaysOfGettingFood } from '../constants/waysOfGettingFood.constant';

const initialState: any = Map({
  items: [],
  waysOfGettingFood: WaysOfGettingFood.DELIVERY,
});

export default (state = initialState, { type, payload }: any) => {
  let items = state.get('items');
  
  switch(type) {
    case ADD_TO_BASKET:
      if (!payload.quantity) {
        payload.quantity = 0;
      }

      payload.quantity++;

      const p = items.find((v: any) =>  v === payload);

      if (!p) {
        return state.set('items', items.concat(payload));
      } 
      
      return state.set('items', [].concat(items));

    case REMOVE_FROM_BASKET:
      const index = items.indexOf(payload);
       
      if (index !== -1) {
        delete payload.quantity;
        items.splice(index, 1);
      }

      return state.set('items', [].concat(items));

    case CHANGE_WAY_OF_GETTING_FOOD:
      return state.set('waysOfGettingFood', payload);
    default:
      return state;
  }
}
