import { RECEIVE_BUSINESS_HOURS_DATA } from '../actions/businessHours.action';

const initialState = {};

export default (state = initialState, {type, payload}: any) => {
  switch (type) {
    case RECEIVE_BUSINESS_HOURS_DATA:
      return {...state, ...payload}
    default:
      return state;
  }
}