import { RECEIVE_SHOP_DATA } from '../actions/shopData.action';

const initialState = {};

export default (state = initialState, { type, payload }: any) => {
  switch (type) {
    case RECEIVE_SHOP_DATA:
      return { ...state, ...payload };
    default:
      return state;
  }
}
