import { combineReducers } from 'redux';
import shopData from './shopData.reducer';
import basket from './basket.reducer';
import showLoader from './loader.reducer';
import userInfo from './userinfo.reducer';
import businessHours from './businessHours.reducer';

export default combineReducers({
  shopData,
  basket,
  showLoader,
  userInfo,
  businessHours,
});
