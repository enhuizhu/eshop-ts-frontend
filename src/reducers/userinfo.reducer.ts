import { RECEIVE_USERINFO, REMOVE_USERINFO } from '../actions/userinfo.action';

const storageKey = 'userInfo';

const getUserInfoFromStorage = () => {
  let userInfo = sessionStorage.getItem(storageKey) || '';

  if (userInfo) {
    try {
      return JSON.parse(userInfo);
    } catch (e) {
      return {};
    }
  }

  return {};
}

const saveUserInfoInStorage = (userInfo: any) => {
  sessionStorage.setItem(storageKey, JSON.stringify(userInfo));
}

const removeUserInfoFromStorage = () => {
  sessionStorage.removeItem(storageKey);
}

const initialState = getUserInfoFromStorage();

export default (state = initialState, { type, payload }: any) => {
  switch(type) {
    case RECEIVE_USERINFO:
      const userInfo = {...state, ...payload };
      saveUserInfoInStorage(userInfo)
      return userInfo;
    case REMOVE_USERINFO:
      removeUserInfoFromStorage();
      return {};
    default:
      return state;
  }
}