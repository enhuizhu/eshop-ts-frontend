import React from 'react';
import { useState } from 'react';
import CommonLayout from '../../layouts/Common.layout';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { layout } from '../../jss/layout';
import { Card, TextField, Button } from '@material-ui/core';
import { base } from '../../jss/base';
import { ApiService } from '../../services/ApiService';
import { showLoader, hideLoader } from '../../actions/loader.actions';
import { SUCCESS, ERROR, NOTIFICATION } from '../../constants/notification.constant';
import  NotificationService  from '../../services/NotificationService';
import store from '../../store/store';

const useStyles = makeStyles((theme: Theme) =>(
  createStyles(
    {
      ...(layout(theme)),
      ...base,
      form: {
        maxWidth: 500,
        width: '90vh',
        margin: '0 auto',
      },
      buttonContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      }
    },
  )
));

export const Register = () => {
  const classes = useStyles();
  const defaultUserDetail: any = {
    username: '',
    email: '',
    password: '',
    repeatPassword: '',
  };
  const defaultErrors = {
    username: false,
    email: false,
    password: false,
    repeatPassword: false,
  };
  let [userDetail, setUserDetail] = useState(defaultUserDetail);
  
  const setUserDetailBaseOnKeyAndValue = (key: any, value: any) => {
    const newUserDetail = Object.assign({}, userDetail, {[key]: value});
    setUserDetail(newUserDetail);
  }

  let [errors, setErrors] = useState(defaultErrors);

  const register = () => {
    // need to check if user detail is valid or not
    console.log('userDetail', userDetail)
    if (!userDetail.username) {
      setErrors({...errors, username: true});
      return ;
    }

    if (!userDetail.email || !/.+@.+/.test(userDetail.email)) {
      setErrors({...errors, email: true});
      return ;
    }

    if (userDetail.password !== userDetail.repeatPassword) {
      NotificationService.pub(NOTIFICATION, {
        msg:  'passwords are not match.',
        msgType: ERROR,
      });
      setErrors({...errors, password: true, repeatPassword: true});
      return ;
    }

    store.dispatch(showLoader());

    ApiService.register(userDetail).then(res => {
      if (res.success) {
        NotificationService.pub(NOTIFICATION, {
          msg:  'you has been registered in the system successfully. Please login to the system.',
          msgType: SUCCESS,
        });
      } else {
        NotificationService.pub(NOTIFICATION, {
          msg: 'there were errors happened during the process of registration',
          msgType: ERROR,
        });
      }
    }).catch(e => {
      NotificationService.pub(NOTIFICATION, {
        msg: 'there were errors happened during the process of registration',
        msgType: ERROR,
      });
    }).finally(() => {
      store.dispatch(hideLoader());
    }); 
  };

  const cancel = () => {
    setUserDetail({...defaultUserDetail});
    setErrors({...defaultErrors});
  };

  return <CommonLayout>
    <div className={classes.menu + ' ' + classes.form}>
      <Card variant="outlined">
         <div className={classes.menuTitle}>
           Register
         </div>
         <div className={classes.padding10}>
            <TextField
              label="Username*"
              placeholder="user name"
              fullWidth
              error={errors['username']}
              margin="normal"
              value={userDetail.username}
              onFocus={() => {
                setErrors({...errors, username: false})
              }}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('username', e.target.value);
              }}
            >
            </TextField>
            <TextField
              label="Email*"
              placeholder="Email"
              error={errors['email']}
              fullWidth
              margin="normal"
              value={userDetail.email}
              onFocus={() => {
                setErrors({...errors, email: false})
              }}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('email', e.target.value);
              }}
            >
            </TextField>
            <TextField
              label="Password*"
              placeholder="password"
              error={errors['password']}
              fullWidth
              margin="normal"
              type="password"
              onFocus={() => {
                setErrors({...errors, password: false})
              }}
              value={userDetail.password}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('password', e.target.value);
              }}
            >
            </TextField>
            <TextField
              label="RepeatPassword*"
              placeholder="repeat password"
              error={errors['repeatPassword']}
              fullWidth
              margin="normal"
              type="password"
              value={userDetail.repeatPassword}
              onFocus={() => {
                setErrors({...errors, repeatPassword: false})
              }}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('repeatPassword', e.target.value);
              }}
            >
            </TextField>
            <div className={classes.buttonContainer}>
              <Button color="primary" variant="contained" onClick={register} type="submit">Register</Button>
              <Button color="secondary" variant="contained" onClick={cancel}>Cancel</Button>
            </div>
         </div>
      </Card>
    </div>
  </CommonLayout>
};

