import React, { Component, PureComponent } from 'react';
import { withStyles, createStyles, Theme } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';;
import { connect } from 'react-redux';
import { UtilService } from '../../services/UtilService';
import { DetailMenuContainer } from '../../components/detail-menu/DetailMenu';
import { BasketContainer } from '../../components/basket/Basket';
import { CategoryList } from '../../components/categories-list/CategoryList';
import { Footer } from '../../components/footer/Footer';
import Card from '@material-ui/core/Card';
import { layout } from '../../jss/layout';
import { base } from '../../jss/base';
import { Header } from '../../components/header/Header';
import { withRouter } from 'react-router';
import { isEmpty } from 'lodash';
import NotificationService from '../../services/NotificationService';
import { WARNING, NOTIFICATION } from '../../constants/notification.constant';
import CommonLayout from '../../layouts/Common.layout';
import OpenHours from '../../components/open-hours/OpenHours';

const ws = withStyles((theme: Theme) => ({
  ...layout(theme),
  ...base,
  contentLayout: {
    display: 'grid',
    'grid-template-areas': `
      'leftCategories menu menu basket'
    `,
    [theme.breakpoints.down('sm')]: {
      'grid-template-areas': `
        'leftCategories' 
        'menu' 
        'basket'
      `,
      gridTemplateColumns: '100%',
    },
    gridTemplateColumns: '25% 25% 25% 25%',
  }
}));

class Home extends PureComponent<any, any> {  
  render() {
    const { classes, shopData } = this.props;
    const categories = UtilService.getValueBaseProperty(shopData, ['categories']);
    const logo = UtilService.getValueBaseProperty(shopData, ['logo']);

    return (
      <CommonLayout>
        <div className={classes.contentLayout}>
          <div className={`${classes.leftCategories} ${classes.mainContentItem}`}>
            <OpenHours></OpenHours>
            
            <CategoryList 
              categories={categories} 
              className={classes.marginTop10}>
            </CategoryList>
          </div>
          <div className={`${classes.menu}`}>
            <Card variant="outlined"
            >
              <div className={classes.menuTitle}>
                Menu
              </div>
              <div className={classes.padding10}>
                {
                  categories &&
                  categories.map((c: any, index: number) => {
                    return <DetailMenuContainer category={c} key={index}></DetailMenuContainer>
                  })
                }
              </div>
            </Card>
          </div>
          <div className={`${classes.basket} ${classes.mainContentItem}`}>
            <BasketContainer buttonText='Order Now' buttonClick={() => {
              if (isEmpty(this.props.userInfo)) {
                NotificationService.pub(NOTIFICATION, {
                  msg: 'Please login first',
                  msgType: WARNING
                });
                // this.headerRef.current.openLoginModal();  
                // console.log('headerRef:', this.headerRef);
              } else if (this.props.basket.get('items').length <= 0) {
                NotificationService.pub(NOTIFICATION, {
                  msg: 'Please select foods first',
                  msgType: WARNING
                });  
              } else {
                this.props.history.push('/order');
              }
            }}></BasketContainer>
          </div>  
        </div>
      </CommonLayout>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    shopData: state.shopData,
    userInfo: state.userInfo,
    basket: state.basket
  }
}

const HomeContainer = connect(mapStateToProps)(withRouter(Home));

export default ws(HomeContainer);

