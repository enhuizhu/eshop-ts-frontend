import React from 'react';
import { createStyles, Theme, makeStyles, useTheme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import { layout } from '../../jss/layout';
import { base } from '../../jss/base';;
import { Header } from '../../components/header/Header';
import { BasketContainer } from '../../components/basket/Basket';
import { Footer } from '../../components/footer/Footer';
import { Breadcrumbs, Typography, Link, TextField, Grid, Stepper, Step, StepLabel, useMediaQuery, Button } from '@material-ui/core';
import store from '../../store/store';
import NotificationService from '../../services/NotificationService';
import { ApiService } from '../../services/ApiService';
import { showLoader, hideLoader } from '../../actions/loader.actions';
import { SUCCESS, ERROR, NOTIFICATION } from '../../constants/notification.constant';
import OpenHours from '../../components/open-hours/OpenHours';

const useStyles = makeStyles((theme: Theme) => createStyles({
  ...(layout(theme)),
  ...base,
  stepper: {
    width: '90vw',
    background: 'transparent',
    '& .MuiStepLabel-label': {
      color: 'white',
    }
  }
}));

const steps = ['Select Food', 'Your Detail', 'Place Order'];

export const Order = (props: any) => {
  const classes = useStyles();
  const defaultUserDetail: any = {};
  const [activeStep, setActiveStep] = React.useState(1);
  const [userDetail, setUserDetail] = React.useState(defaultUserDetail);
  const state = store.getState();
  const items = state.basket.get('items');
  const defaultError: any = {};
  let [errors, setErrors] = React.useState(defaultError);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('sm'));
  
  if (items.length <= 0) {
    setTimeout(() => {
      NotificationService.pub(NOTIFICATION, {
        msg: 'basket can not be empty, will redirect you to food selection page, thanks.',
        msgType: ERROR
      });  
    }, 10);
    
    setTimeout(() => {
      props.history.push('/');
    }, 3000);
  } 

  const setUserDetailBaseOnKeyAndValue = (key: any, value: any) => {
    const newUserDetail = Object.assign({}, userDetail, {[key]: value});
    setUserDetail(newUserDetail);
  }
  
  const placeOrderHandler = () => {
    // need to check if anything is empty
    const theKeysNeedToCheck = ['tel', 'address1', 'city', 'postcode'];

    let hasError = false;
    
    theKeysNeedToCheck.forEach(k => {
      if (!userDetail[k]) {
        errors[k] = true;
        hasError = true;
      }
    });

    if (hasError) {
      setErrors({...errors});
      return ;
    }

    if (!hasError) {
      console.log('items', items, userDetail);
      let orderInfo = {
        note: userDetail.note,
        items: items.map((item: any) => ({id: item.id, quantity: item.quantity})),
        address: {
          ...userDetail,
        }
      }

      store.dispatch(showLoader());

      ApiService.placeOrder(orderInfo).then(res => {
        if (res.success) {
          NotificationService.pub(NOTIFICATION, {
            msg:  'Order has been placed successfully.',
            msgType: SUCCESS,
          });
        } else {
          NotificationService.pub(NOTIFICATION, {
            msg: 'there were errors happened during the process of placing order',
            msgType: ERROR,
          });
        }
      }).catch(e => {
        NotificationService.pub(NOTIFICATION, {
          msg: 'there were errors happened during the process of placing order',
          msgType: ERROR,
        });
      }).finally(() => {
        store.dispatch(hideLoader());
      });
    }
  }

  return (
    <div className={classes.appContainer}>
      <div className={classes.header}>
        <Header>
          <h1>Place Your Order</h1>
          <Stepper activeStep={activeStep} className={classes.stepper}>
            { 
              steps.map((label, index) => {
                const stepProps: { completed?: boolean } = {};
                
                return <Step key={label} {...stepProps}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              })
            }
          </Stepper>
        </Header>
      </div>
      <div className={classes.breadcrumbs}>
        <Breadcrumbs aria-label="breadcrumb" style={{cursor: 'pointer'}}>
          <Link color="inherit" onClick={() => {
            console.log('link has been clicked!');
            props.history.push('/');
          }}>
            Home
          </Link>
          <Typography color="textPrimary">Your Detail</Typography>
        </Breadcrumbs>
      </div>
      <div className={`${classes.leftCategories} ${classes.mainContentItem}`}>
        <OpenHours></OpenHours>
      </div>
      <div className={`${classes.menu}`}>
        <Card variant="outlined">
          <div className={classes.menuTitle}>
            Your order details
          </div>
          <div className={classes.padding10}>
            {/* <TextField
              label="Required*"
              placeholder="First Name"
              fullWidth
              margin="normal"
              defaultValue={userDetail.firstName}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('firstName', e.target.value);
              }}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              label="Required*"
              placeholder="Last Name"
              fullWidth
              margin="normal"
              defaultValue={userDetail.lastName}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('lastName', e.target.value);
              }}
              InputLabelProps={{
                shrink: true,
              }}
            /> */}
            <TextField
              label="Required*"
              placeholder="Telephone/mobile"
              fullWidth
              margin="normal"
              defaultValue={userDetail.tel}
              error={errors.tel}
              onFocus={() => {
                setErrors({...errors, tel: false})
              }}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('tel', e.target.value);
              }}
              InputLabelProps={{
                shrink: true,
              }}
            />
            {/* <TextField
              label="Required*"
              placeholder="Email"
              fullWidth
              margin="normal"
              defaultValue={userDetail.email}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('email', e.target.value);
              }}
              InputLabelProps={{
                shrink: true,
              }}
            /> */}
            <TextField
              label="Required*"
              placeholder="Your first line of address"
              fullWidth
              margin="normal"
              defaultValue={userDetail.address1}
              error={errors.address1}
              onFocus={() => {
                setErrors({...errors, address1: false})
              }}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('address1', e.target.value);
              }}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              label="Required*"
              placeholder="Your second line of address"
              fullWidth
              margin="normal"
              defaultValue={userDetail.address2}
              error={errors.address1}
              onFocus={() => {
                setErrors({...errors, address1: false})
              }}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('address2', e.target.value);
              }}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6}>
                <TextField
                  label="Required*"
                  placeholder="City"
                  margin="normal"
                  fullWidth
                  defaultValue={userDetail.city}
                  error={errors.city}
                  onChange={(e) => {
                    setUserDetailBaseOnKeyAndValue('city', e.target.value);
                  }}
                  onFocus={() => {
                    setErrors({...errors, city: false})
                  }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  label="Required*"
                  placeholder="Post code"
                  margin="normal"
                  fullWidth
                  defaultValue={userDetail.postcode}
                  error={errors.postcode}
                  onChange={(e) => {
                    setUserDetailBaseOnKeyAndValue('postcode', e.target.value);
                  }}
                  onFocus={() => {
                    setErrors({...errors, postcode: false})
                  }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
            </Grid>
            <TextField
              label="Notes for the restaurant"
              placeholder="Ex. Allergies, cash change..."
              multiline
              fullWidth
              rows="4"
              defaultValue={userDetail.note}
              onChange={(e) => {
                setUserDetailBaseOnKeyAndValue('note', e.target.value);
              }}
            />
          </div>
        </Card>
      </div>
      <div className={`${classes.basket} ${classes.mainContentItem}`}>
        <BasketContainer className="basket" buttonText='Place Order' buttonClick={placeOrderHandler}></BasketContainer>
        {/* {
          matches ? <Button variant="contained" color="primary">Open Basket</Button> :
            <BasketContainer className="basket" buttonText='Place Order' buttonClick={placeOrderHandler}></BasketContainer>
        } */}
        
      </div>
      <div className={classes.footer}>
        <Footer></Footer>
      </div>
    </div>
  );
};

